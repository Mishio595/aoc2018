open Core

exception Non_Overlapping_Claim of string

let id = Tyre.(compile (str"#" *> pos_int))
let offset = Tyre.(compile (str" @ " *> pos_int <&> str"," *> pos_int))
let size = Tyre.(compile (str": " *> pos_int <&> str"x" *> pos_int))

let parse s =
    let r_id = Tyre.exec id s in
    let r_offset = Tyre.exec offset s in
    let r_size = Tyre.exec size s in
    match (r_id, r_offset, r_size) with
    | (Result.Ok id, Result.Ok (off_x, off_y), Result.Ok (size_x, size_y)) ->
        (string_of_int id, off_x, off_y, size_x, size_y)
    | _ -> failwith "Invalid Claim"

let rec parse_lines acc lines =
    match lines with
    | [] -> acc
    | hd :: tl -> parse_lines (parse hd :: acc) tl

let rec find_max acc lines =
    match lines with
    | [] -> acc
    | (_, offx, offy, sx, sy) :: tl ->
        let max_x, max_y = acc in
        if max_x < offx + sx && max_y < offy + sy then
            find_max (offx + sx, offy + sy) tl
        else if max_x < offx + sx then
            find_max (offx + sx, max_y) tl
        else if max_y < offy + sy then
            find_max (max_x, offy + sy) tl
        else
            find_max (max_x, max_y) tl

let rec populate_canvas canvas lines =
    match lines with
    | [] -> canvas
    | (_, offx, offy, sx, sy) :: tl ->
        let rec populate_claim offx offy sx sy ox =
            let (posx, posy) = (offx + sx - 1, offy + sy - 1) in
            canvas.(posx).(posy) <- canvas.(posx).(posy) + 1;
            match (sx, sy) with
            | (1, 1) -> ()
            | (1, y) -> populate_claim offx offy ox (y - 1) ox
            | (x, y) -> populate_claim offx offy (x - 1) y ox
        in
        populate_claim offx offy sx sy sx;
        populate_canvas canvas tl

let rec evaluate_canvas canvas lines =
    match lines with
    | [] -> canvas
    | (id, offx, offy, sx, sy) :: tl ->
        let status = ref true in
        let rec evaluate_claim offx offy sx sy ox =
            let (posx, posy) = (offx + sx - 1, offy + sy - 1) in
            let cur = canvas.(posx).(posy) in
            (match cur with
            | 1 -> ()
            | _ -> status := false
            );
            match (sx, sy) with
            | (1, 1) -> !status
            | (1, y) -> evaluate_claim offx offy ox (y - 1) ox
            | (x, y) -> evaluate_claim offx offy (x - 1) y ox
        in
        match evaluate_claim offx offy sx sy sx with
        | true -> raise (Non_Overlapping_Claim id)
        | false -> ();
        evaluate_canvas canvas tl

let _ =
    let parsed = In_channel.read_lines "input.sol"
    |> parse_lines []
    in
    let dimx, dimy = find_max (0,0) parsed in
    "Max x : " ^ string_of_int dimx ^ " Max y: " ^ string_of_int dimy
    |> print_endline;
    let canvas = (Array.make_matrix ~dimx ~dimy 0
    |> populate_canvas) parsed in
    try
        evaluate_canvas canvas parsed |> ignore
    with Non_Overlapping_Claim s -> print_endline s