open Core

module InstructionMap = Map.Make(Char)

type worker = {
    task: char;
    has_task: bool;
    time_left: int;
}

exception Assigned of worker list

let assign_worker workers task =
    let rec loop a l = match l with
        | [] -> a
        | hd :: tl -> match hd.has_task with
            | false -> 
                let w = {has_task=true; task=task; time_left=(Char.to_int task)-4} in
                raise (Assigned (List.append (w::a) tl))
            | true -> loop (hd::a) tl
    in try loop [] workers with Assigned w -> w


let max_workers = 5

let rec generator ~init l = match l with
    | [] -> init
    | hd :: tl ->
        let (res, id) = Parser.parse hd in
        let init = InstructionMap.update ~f:(function Some i -> i | None -> []) init res in
        let init = InstructionMap.update ~f:(fun i -> match i with
        | Some i -> res :: i
        | None -> res :: []
        ) init id
        in generator ~init tl

let update_workers workers =
    let rec loop w t a = match w with
    | [] -> a, t
    | w::tl ->
        let w,t = if w.time_left = 0 then begin
            let t = if w.has_task then w.task::t else t in
            {task='_'; has_task=false; time_left=0}, t
        end else w,t in loop tl t (w::a)
    in
    let workers = List.map ~f:(fun w -> {w with time_left = Int.max 0 w.time_left-1;}) workers |> List.rev in
    loop workers [] []

let rec remove_from_map keys m =
    match keys with
    | [] -> m
    | hd::tl -> remove_from_map tl (InstructionMap.remove m hd)

let rec evaluate ~workers ~time m =
    match InstructionMap.is_empty m with
    | true -> time
    | false -> begin
        let (workers, tasks) = update_workers workers in
        let m = remove_from_map tasks m in
        let (workers, m) = match List.for_all ~f:(fun w -> w.has_task) workers with
        | true -> workers, m
        | false -> begin
            let w = ref workers in
            let m = InstructionMap.filter_mapi ~f:(fun ~key ~data ->
                if List.for_all ~f:(fun w -> w.task <> key) workers then (
                match List.for_all ~f:(fun c -> not (InstructionMap.mem m c)) data with
                | true -> w := assign_worker !w key; Some data
                | false -> Some data
                ) else Some data
            ) m
            in !w,m
        end in
        evaluate ~workers ~time:(time+1) m
    end

let _ =
    let workers =
        let rec gen a c m = match c<m with
        | false -> a
        | true -> gen ({task = '_'; has_task = false; time_left = 0;}::a) (c+1) m
        in gen [] 0 max_workers
    in
    In_channel.read_lines "input.sol"
    |> generator ~init:InstructionMap.empty
    |> evaluate ~workers ~time:(-1)
    |> string_of_int
    |> print_endline