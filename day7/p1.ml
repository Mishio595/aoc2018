open Core

module InstructionMap = Map.Make(Char)

let rec generator ~init l = match l with
    | [] -> init
    | hd :: tl ->
        let (res, id) = Parser.parse hd in
        let init = InstructionMap.update ~f:(function Some i -> i | None -> []) init res in
        let init = InstructionMap.update ~f:(fun i -> match i with
        | Some i -> res :: i
        | None -> res :: []
        ) init id
        in generator ~init tl

let rec evaluate ~init m =
    match InstructionMap.is_empty m with
    | true -> init
    | false -> begin
        let cont = ref true in
        InstructionMap.filter_mapi ~f:(fun ~key ~data ->
            match !cont && List.for_all ~f:(fun c -> not (InstructionMap.mem m c)) data with
            | true -> Queue.enqueue init key; cont := false; None
            | false -> Some data
        ) m
        |> evaluate ~init
    end

let _ =
    In_channel.read_lines "input.sol"
    |> generator ~init:InstructionMap.empty
    |> evaluate ~init:(Queue.create ())
    |> Queue.to_list
    |> String.of_char_list
    |> print_endline