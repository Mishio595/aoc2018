open Core

let line_parse = Tyre.(compile (
    str "Step " *> pcre "\\w" <* str " must be finished " <&> str "before step " *> pcre "\\w" <* str " can begin."
))

let parse s = match Tyre.exec line_parse s with
    | Ok (req, ins) -> (Char.of_string req, Char.of_string ins)
    | _ -> failwith "Failed to parse"