open Core

let sum l = List.fold ~init:0 ~f:(+) l

let inspect_int ?(pre="") i = print_endline (pre ^ string_of_int i); i

let get_node_count n =
    let rec get_child_count m (n:Parser.node) = match List.nth (List.rev n.children) (m-1) with
        | Some c -> check_metadata c 0
        | None -> 0
    and check_metadata (Parser.Node node) acc =
        let nc, _ = node.headers in
        if nc = 0 then acc + (sum node.metadata) else begin
        List.fold ~init:acc ~f:(fun a m ->
            a + get_child_count m node
        ) node.metadata end
    in check_metadata n 0

let _ =
    let tree, _ = Parser.parse () in
    get_node_count tree
    |> string_of_int
    |> print_endline