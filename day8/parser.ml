open Core

type t = Node of node
and node = {
    headers: int * int;
    children: t list;
    metadata: int list;
} [@@deriving sexp]

let input = match Sys.getenv "INPUT" with
    | Some s -> s
    | None -> failwith "No input"

let take_n n l =
    let rec loop n l c a = if c < n then
        match l with
        | [] -> a, l
        | hd::tl -> loop n tl (c+1) (hd::a)
        else a, l
    in loop n l 0 []

let rec take_child l = match l with
    | [] | _::[] -> failwith "Insufficient list length"
    | c::m::tl ->
        let children, tl = take_n_children c tl in
        let metadata, tl = take_n m tl in
        Node { headers = c,m; children; metadata; }, tl
and take_n_children n l =
    let rec loop n l c a = if c < n then
        match l with
        | [] -> a, l
        | l ->
            let child, tl = take_child l in
            loop n tl (c+1) (child::a)
        else a, l
    in loop n l 0 []

let parse () =
    In_channel.read_all input
    |> String.strip
    |> String.split ~on:' '
    |> List.map ~f:(int_of_string)
    |> take_child