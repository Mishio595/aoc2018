open Core

let sum l = List.fold ~init:0 ~f:(+) l

let rec sum_metadata (Parser.Node n) a =
    let a = a + (sum n.metadata) in
    let rec sum_all_children l c = match l with
        | [] -> c
        | hd::tl -> sum_all_children tl (sum_metadata hd c)
    in
    sum_all_children n.children a

let _ =
    let tree, _ = Parser.parse () in
    sum_metadata tree 0
    |> string_of_int
    |> print_endline