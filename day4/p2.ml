open Core

let line_parser = Tyre.(compile (str "[" *> pcre ".*?" <* str "]" <&> pcre ".*"))
let guard_id_from_action = Tyre.(compile (str "#" *> pos_int))

let rec get_counts a l =
    let equal = (=) in
    match l with
    | [] -> a
    | hd :: tl -> begin
        let a = match List.Assoc.find ~equal a hd with
        | Some s -> List.Assoc.add ~equal a hd (s+1)
        | None -> List.Assoc.add ~equal a hd 1
        in
        get_counts a tl
    end

let rec gen_range a b l =
    if a >= b then l
    else gen_range (a + 1) b (a :: l)

let gen_time_range tnew told =
    let low = Time.(to_ofday ~zone:(Zone.of_utc_offset ~hours:(-7)) told |> Ofday.to_parts).min in
    let high = Time.(to_ofday ~zone:(Zone.of_utc_offset ~hours:(-7)) tnew |> Ofday.to_parts).min in
    if low > high then
        gen_range low 59 []
        |> gen_range 0 high
    else
        gen_range low high []

module Action = struct
    type t =
        | Start_shift of int
        | Sleep
        | Wake
    
    let of_string = function
        | "wakes up" -> Wake
        | "falls asleep" -> Sleep
        | a -> begin
            match Tyre.exec guard_id_from_action a with
            | Result.Ok id -> Start_shift id
            | _ -> failwith "Invalid Guard ID"
        end

    let to_string = function
        | Start_shift id -> "Guard #" ^ string_of_int id ^ " started shift"
        | Wake -> "Woke up"
        | Sleep -> "Went to sleep"
end

let parse_line line =
    let (time, action) = match Tyre.exec line_parser line with
    | Result.Ok (t, a) -> (t, String.strip a)
    | _ -> failwith "Invalid Line"
    in
    (Time.of_string time, Action.of_string action)

let rec parse_lines acc lines =
    match lines with
    | [] -> acc
    | hd :: tl ->
        parse_lines (parse_line hd :: acc) tl

let rec evaluate_data entries last_guard last_action state =
    let open Action in
    match entries with
    | [] -> state
    | (time, action) :: tl ->
        let id, this_action = match action with
        | Start_shift id -> (id, None)
        | Wake -> (last_guard, Some (`Wake time))
        | Sleep -> (last_guard, Some (`Sleep time))
        in
        let state_update = match this_action with
        | Some `Wake t_new -> begin
            let t_old = match last_action with
            | Some `Sleep t -> t
            | _ -> failwith "Bad state"
            in
            let span = Time.diff t_new t_old in
            let range = gen_time_range t_new t_old in
            match List.Assoc.find ~equal:(=) state id with
            | Some (tot, minutes) -> begin
                List.Assoc.add ~equal:(=) state id (Time.Span.(tot + span), range :: minutes)
            end
            | None -> List.Assoc.add ~equal:(=) state id (Time.Span.zero, range :: [])
        end
        | _ -> state
        in
        evaluate_data tl id this_action state_update

let _ =
    let lines = In_channel.read_lines "input.sol" in
    let parsed = parse_lines [] lines in
    let sorted = List.sort ~compare:(fun (t1,_) (t2,_) -> Time.compare t1 t2) parsed in
    let evaluated_state = evaluate_data sorted (-1) None [] in
    let highest_count = ref 0 in
    let minute_of_highest_count = ref (-1) in
    let a_id = ref (-1) in
    List.iter ~f:(fun (id, (_, minutes)) ->
        let counts = get_counts [] (List.concat minutes) in
        let max = ref 0 in
        let a_min = ref (-1) in
        List.iter ~f:(fun (minute, amount) ->
            if amount > !max then begin
                max := amount;
                a_min := minute;
            end
        ) counts;
        if !max > !highest_count then (
            highest_count := !max;
            minute_of_highest_count := !a_min;
            a_id := id;
        )
    ) evaluated_state;
    "Guard ID: " ^ string_of_int !a_id ^ "\n" ^
    "Minute: " ^ string_of_int !minute_of_highest_count ^ " with max " ^ string_of_int !highest_count ^ "\n" ^
    "Answer: " ^ string_of_int (!a_id * !minute_of_highest_count)
    |> print_endline