open Core

let range = List.range ~start:`exclusive ~stop:`inclusive

let generate_grid ~serial_number size_x size_y =
    let x, y = range 0 size_x, range 0 size_y in
    List.cartesian_product x y
    |> List.map ~f:(fun (x,y) ->
        let rack_id = x + 10 in
        let intermediate = ((rack_id * y) + serial_number) * rack_id in
        let h = (Int.to_string intermediate
        |> String.to_list
        |> List.rev
        |> List.nth) 2
        in
        let n = match h with
        | Some n -> String.of_char n |> int_of_string
        | None -> 0
        in (x,y), n - 5)

let highest_power_zone serial_number dimx dimy =
    let power_grid = generate_grid ~serial_number dimx dimy in
    let grid_array = Array.make_matrix ~dimx ~dimy 0 in
    List.iter ~f:(fun ((x,y),pow) -> grid_array.(x-1).(y-1) <- pow) power_grid;
    let res = ref (0,0,0,0) in
    for size=1 to 299 do
        for y=0 to 299-size do
            for x=0 to 299-size do
                let pow = ref 0 in
                for y1=0 to size-1 do
                    for x1=0 to size-1 do
                        pow := !pow + grid_array.(x+x1).(y+y1)
                    done;
                done;
                let hi,_,_,_ = !res in
                if !pow > hi then res := !pow,x+1,y+1,size
            done;
        done;
    done;
    let total,x,y,size = !res in printf "Total: %d - %d, %d, %d\n" total x y size

let _ = highest_power_zone 6303 300 300