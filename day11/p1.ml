open Core

let range = List.range ~start:`exclusive ~stop:`inclusive

let generate_grid ~serial_number size_x size_y =
    let x, y = range 0 size_x, range 0 size_y in
    List.cartesian_product x y
    |> List.map ~f:(fun (x,y) ->
        let rack_id = x + 10 in
        let intermediate = ((rack_id * y) + serial_number) * rack_id in
        let h = (Int.to_string intermediate
        |> String.to_list
        |> List.rev
        |> List.nth) 2
        in
        let n = match h with
        | Some n -> String.of_char n |> int_of_string
        | None -> 0
        in (x,y), n - 5)

let coords_of_high_power serial_number dimx dimy =
    let power_grid = generate_grid ~serial_number dimx dimy in
    let grid_array = Array.make_matrix ~dimx ~dimy 0 in
    List.iter ~f:(fun ((x,y),pow) -> grid_array.(x-1).(y-1) <- pow) power_grid;
    let total, cell = Array.foldi ~init:(0,(0,0)) ~f:(fun y init row ->
        Array.foldi ~init ~f:(fun x (tot,cor) _ ->
        let sum = if x+2 < dimx && y+2 < dimy then begin
            List.cartesian_product (List.range 0 3) (List.range 0 3)
            |> List.map ~f:(fun (x1,y1) -> grid_array.(x+x1).(y+y1))
            |> List.fold ~init:0 ~f:(+)
        end else 0 in
        if sum > tot then (sum, (x+1,y+1))
        else (tot,cor)
        ) row
    ) grid_array in
    let x,y = cell in
    printf "Total: %d - %d, %d\n" total x y

let _ = coords_of_high_power 6303 300 300