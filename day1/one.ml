open Core

let read file = In_channel.read_lines file

let rec check res i =
    match res with
    | [] -> false
    | hd :: tl ->
        if hd = i then true else check tl i

let lines = read "input.sol"

let rec main base acc =
    let stat = ref (false,0) in
    let rec loop l acc =
        match l with
        | [] -> acc
        | hd :: tl ->
            let a = match String.prefix hd 1 with
            | "+" -> begin
                match String.chop_prefix ~prefix:"+" hd with
                | Some s -> int_of_string s
                | None -> failwith "Invalid prefix"
            end
            | "-" -> begin
                match String.chop_prefix ~prefix:"-" hd with
                | Some s -> 0 - (int_of_string s)
                | None -> failwith "Invalid prefix"
            end
            | _ -> failwith "Invalid prefix"
            in
            let v = match acc with
            | [] -> base + a
            | hd :: _ -> hd + a
            in
            match check acc v with
            | true -> stat := (true, v); acc
            | false ->
                stat := (false, v);
                let acc = v :: acc in
                loop tl acc
    in
    let res = loop lines acc in
    match !stat with
    | (true,a) -> "Result: " ^ string_of_int a |> print_endline;
    | (false,a) -> main a res

let _ = main 0 []
