open Core

exception Out_of_bounds
exception Matched of char option array
exception No_dups of char array

let compare a b = match a, b with
    | Some c1, Some c2 -> Char.(lowercase c1 = lowercase c2) && c1 <> c2
    | _ -> failwith "Passed none"

let _ =
    let input = match In_channel.read_lines "input.sol" with
    | [] -> failwith "Invalid Input"
    | hd :: _ -> String.to_array hd |> Array.map ~f:(fun c -> Some c)
    in
    let rec traverse a i =
        if i+1 >= Array.length a then raise Out_of_bounds
        else
        match compare a.(i) a.(i+1) with
        | true -> begin
            a.(i) <- None;
            a.(i+1) <- None;
            raise @@ Matched a
        end
        | false -> traverse a (i+1)
    in
    let rec meta a =
        try traverse a 0
        with
        | Out_of_bounds -> raise @@ No_dups (Array.filter_opt a)
        | Matched a -> meta Array.(filter_opt a |> map ~f:(fun c -> Some c))
    in
    try meta input
    with No_dups a ->
    Array.length a
    |> string_of_int
    |> print_endline