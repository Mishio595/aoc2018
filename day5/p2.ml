open Core

exception Out_of_bounds
exception Matched of char option array
exception No_dups of char array

let alphabet = ['a';'b';'c';'d';'e';'f';'g';'h';'i';'j';'k';'l';'m';'n';'o';'p';'q';'r';'s';'t';'u';'v';'w';'x';'y';'z';]

let compare a b = match a, b with
    | Some c1, Some c2 -> Char.(lowercase c1 = lowercase c2) && c1 <> c2
    | _ -> failwith "Passed none"

let rec traverse a i =
    if i+1 >= Array.length a then raise Out_of_bounds
    else
    match compare a.(i) a.(i+1) with
    | true -> begin
        a.(i) <- None;
        a.(i+1) <- None;
        raise @@ Matched a
    end
    | false -> traverse a (i+1)

let rec meta a =
    try traverse a 0
    with
    | Out_of_bounds -> raise @@ No_dups (Array.filter_opt a)
    | Matched a -> meta Array.(filter_opt a |> map ~f:(fun c -> Some c))

let rec metameta a_base m letters =
    match letters with
    | [] -> m
    | hd :: tl -> begin
        print_endline (String.of_char hd ^ ": " ^ string_of_int m);
        try (Array.filter_map ~f:(fun c ->
                if Char.(hd = lowercase c) then None
                else Some c
            ) a_base
            |> Array.map ~f:(fun c -> Some c)
            |> meta)
        with No_dups a -> begin
            let l = Array.length a in
            let count = match l > m with
            | true -> m
            | false -> l
            in
            metameta a_base count tl
        end
    end

let _ =
    let input = match In_channel.read_lines "input.sol" with
    | [] -> failwith "Invalid Input"
    | hd :: _ -> String.to_array hd
    in
    metameta input Int.max_value alphabet
    |> string_of_int
    |> print_endline