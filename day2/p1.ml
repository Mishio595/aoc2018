open Core

module StringSet = Set.Make(String)

let twos = ref StringSet.empty
let threes = ref StringSet.empty
let file = In_channel.read_lines "input.sol"
let equal = (=)

let rec get_counts a l =
    match l with
    | [] -> a
    | hd :: tl -> begin
        let a = match List.Assoc.find ~equal a hd with
        | Some s -> List.Assoc.add ~equal a hd (s+1)
        | None -> List.Assoc.add ~equal a hd 1
        in
        get_counts a tl
    end

let rec main lines =
    match lines with
    | [] -> ()
    | hd :: tl ->
        let slist = String.to_list hd in
        let counts = get_counts [] slist in
        List.iter ~f:(fun (_, count) ->
            if count = 3 then threes := StringSet.add !threes hd
            else if count = 2 then twos := StringSet.add !twos hd
        ) counts;
        main tl

let _ =
    main file;
    (StringSet.length !twos) * (StringSet.length !threes)
    |> string_of_int
    |> print_endline