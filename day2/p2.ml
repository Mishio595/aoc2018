open Core

let compare s1 s2 =
    let open List.Or_unequal_lengths in
    let s1 = String.to_list s1 in
    let s2 = String.to_list s2 in
    let unfiltered = match List.map2 ~f:(fun c1 c2 ->
        match c1 = c2 with
        | true -> Some c1
        | false -> None
    ) s1 s2 with
    | Ok l -> l
    | Unequal_lengths -> failwith "Unequal Lengths"
    in
    let filtered = List.filter_map ~f:(fun c -> c) unfiltered in
    if List.length filtered = (List.length s1 - 1) then
        Some filtered
    else None

let rec main (lines:string list) =
    match lines with
    | [] -> failwith "No matches"
    | hd :: tl -> begin
        let current_i = match List.findi ~f:(fun _ c -> c=hd) lines with
        | Some (i, _) -> i
        | None -> failwith "Self not in list"
        in
        match List.filter_mapi ~f:(fun i s ->
            if current_i = i then None
            else compare s hd
        ) lines with
        | [] -> main tl
        | hd :: _ -> String.of_char_list hd
    end

let _ =
    In_channel.read_lines "input.sol"
    |> main
    |> print_endline