open Core

module IntMap = Map.Make(struct
    type t = int * int [@@deriving sexp]
    let compare = Tuple2.compare ~cmp1:Int.compare ~cmp2:Int.compare
end)

let of_string s = match String.split ~on:',' s with
    | x :: y :: [] -> int_of_string (String.strip x), int_of_string (String.strip y)
    | _ -> raise @@ Invalid_argument s

let get_bounds l =
    let rec loop l r t m b = match l with
    | [] -> m, r, t, b
    | hd::tl ->
        let px, py = hd in
        loop tl (Int.max px r) (Int.max py t) (Int.min px m) (Int.min py b)
    in loop l 0 0 0 0

let parse str =
    In_channel.read_lines str
    |> List.map ~f:(of_string)

let get_closest x1 y1 coords =
    let _, closest = coords
    |> List.map ~f:(fun (x,y) ->
        let dist = abs (x-x1) + abs (y-y1) in
        ((x,y), dist))
    |> List.fold ~init:(None, []) ~f:(fun (opt, l) (coord, dist) ->
        match opt with
        | None -> (Some dist, [coord])
        | Some d -> begin match Int.compare dist d with
            | i when i<0 -> (Some dist, [coord])
            | 0 -> (Some d, coord::l)
            | _ -> (opt, l)
        end)
    in
    if List.length closest = 1 then List.hd closest
    else None

let get_dense_region ?(max_distance=10_000) coords =
    let left, right, top, bottom = get_bounds coords in
    let x,y = List.range left right, List.range bottom top in
    List.cartesian_product x y
    |> List.fold ~init:0 ~f:(fun acc (loc_x, loc_y) ->
        let tot_dist = coords
        |> List.map ~f:(fun (x,y) -> abs (x-loc_x) + abs (y-loc_y))
        |> List.fold ~init:0 ~f:(+)
        in
        if tot_dist < max_distance then acc+1
        else acc)
    

let _ =
    let coords = parse "input.sol" in
    get_dense_region coords
    |> Int.sexp_of_t
    |> print_s