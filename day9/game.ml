open Core

module DL = Doubly_linked
module Scores = Map.Make(Int)

let get_nth ~(mode:[`Left | `Right]) anchor n dl =
    let rec loop a n c f dl =
        if c<n then begin
            let e = match f dl a with
            | Some e -> e
            | None -> match mode with
                | `Right -> DL.(match first_elt dl with Some e -> e | None -> failwith "Bad state")
                | `Left -> DL.(match last_elt dl with Some e -> e | None -> failwith "Bad state")
            in loop e n (c+1) f dl
        end else
            a
    in match mode with
    | `Left -> loop anchor n 0 (DL.prev) dl
    | `Right -> loop anchor n 0 (DL.next) dl

let score cm cp sm state scores =
    let to_remove = get_nth ~mode:`Left cm 7 state in
    let cm = get_nth ~mode:`Right to_remove 1 state in
    DL.remove state to_remove;
    let s = DL.Elt.value to_remove in
    let scores = Scores.update ~f:(function Some c -> c + s + sm | None -> s + sm) scores cp in
    cm, state, scores

let standard_move cm sm state =
    let anchor = get_nth ~mode:`Right cm 1 state in
    let cm = DL.insert_after state anchor sm in
    cm, state

let rec generator
    ?(current_player=1)
    ~current_marble
    ~max_players
    ~max_marbles
    n state scores =
    match n <= max_marbles with
    | false -> scores
    | true  ->
        let current_marble, state, scores = if n % 23 = 0 then
            score current_marble current_player n state scores
        else
            let cm, state = standard_move current_marble n state in
            cm, state, scores
        in
        let current_player = if current_player = max_players then 1 else current_player + 1 in
        generator
            ~current_marble
            ~current_player
            ~max_players
            ~max_marbles
            (n+1) state scores

let _ =
    let max_players = 424 in
    let max_marbles = 71144 in (* Multiply by 100 to do part 2 *)
    let state_init = DL.create () in
    let current_marble = DL.insert_first state_init 0 in
    let scores = generator ~current_marble ~max_players ~max_marbles 1 state_init Scores.empty in
    let player, score = Scores.fold ~init:(0,0) ~f:(fun ~key ~data (p,c) ->
        if data > c then (key, data) else (p,c)
    ) scores in
    "Player " ^ string_of_int player ^ " won with a score of " ^ string_of_int score |> print_endline